﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkUtils
{
    public class ByteBuffer
    {
        public class ByteBufferException : Exception
        {
            public ByteBufferException(string message) : base(message) { }
        }

        private readonly List<byte> bytesList = new List<byte>();

        private byte[] bytesArray;
        private int maxSize;

        public int ReadIndex { get; set; }

        public ByteBuffer(int maxSize = 1024 * 1024)
        {
            this.maxSize = maxSize;
            bytesArray = null;
        }

        public ByteBuffer(ByteBuffer copy)
        {
            bytesList.AddRange(copy.bytesList);
            maxSize = copy.maxSize;
            ReadIndex = copy.ReadIndex;
            bytesArray = null;
        }

        public byte[] ToBytes()
        {
            return bytesList.ToArray();
        }

        public void FromBytes(byte[] bytes, int count, bool clearFirst = false)
        {
            if (clearFirst)
            {
                bytesList.Clear();
            }
            else
            {
                Flush();
            }

            bytesList.AddRange(bytes.ToList().GetRange(0, count));
        }

        public void Append(ByteBuffer buf)
        {
            bytesList.AddRange(buf.bytesList);
            bytesArray = null;
            Flush();
        }

        // Удаление уже прочитанных данных
        private void Flush()
        {
            if (ReadIndex > 0 && bytesList.Count > maxSize)
            {
                bytesArray = bytesList.GetRange(ReadIndex, bytesList.Count - ReadIndex).ToArray();
                bytesList.Clear();
                bytesList.AddRange(bytesArray);
                ReadIndex = 0;
            }
        }

        public int Length
        {
            get
            {
                return bytesList.Count - ReadIndex;
            }
        }

        public void Clear()
        {
            bytesList.Clear();
            ReadIndex = 0;
            bytesArray = null;
        }

        #region WRITE
        public void WriteByte(byte value)
        {
            bytesList.Add(value);
            bytesArray = null;
        }

        public void WriteInteger(int value)
        {
            bytesList.AddRange(BitConverter.GetBytes(value));
            bytesArray = null;
        }

        public void WriteGuid(Guid value)
        {            
            WriteBytes(value.ToByteArray());
        }

        public void WriteFloat(float value)
        {
            bytesList.AddRange(BitConverter.GetBytes(value));
            bytesArray = null;
        }

        public void WriteBool(bool value)
        {
            bytesList.AddRange(BitConverter.GetBytes(value));
            bytesArray = null;
        }

        public void WriteString(string value)
        {
            WriteInteger(value.Length);
            bytesList.AddRange(Encoding.ASCII.GetBytes(value));
        }

        public void WritePackType(NetPacket.PackType type)
        {
            WriteByte((byte)type);
        }

        public void WriteBytes(byte[] bytes)
        {
            bytesList.AddRange(bytes);
            bytesArray = null;
        }
        #endregion

        #region READ
        public byte[] ReadBytes(int count)
        {
            CheckBuffer(count);

            byte[] result = bytesArray.ToList().GetRange(ReadIndex, count).ToArray();
            ReadIndex += count;

            return result;
        }

        public byte ReadByte()
        {
            CheckBuffer(1);

            byte result = bytesArray[ReadIndex];
            ReadIndex += 1;

            return result;
        }

        public Guid ReadGuid()
        {
            return new Guid(ReadBytes(16));
        }

        public int ReadInteger()
        {
            CheckBuffer(4);

            int result = BitConverter.ToInt32(bytesArray, ReadIndex);
            ReadIndex += 4;

            return result;
        }

        public float ReadFloat()
        {
            CheckBuffer(4);

            float result = BitConverter.ToSingle(bytesArray, ReadIndex);
            ReadIndex += 4;

            return result;
        }

        public bool ReadBool()
        {
            CheckBuffer(1);

            return BitConverter.ToBoolean(bytesArray, ReadIndex++);
        }

        public string ReadString()
        {
            int len = ReadInteger();
            if (Length < len)
            {
                throw new ByteBufferException("Net Buffer out of range");
            }

            string result = Encoding.ASCII.GetString(bytesArray, ReadIndex, len);
            ReadIndex += len;

            return result;
        }

        public NetPacket.PackType ReadPackType()
        {
            return (NetPacket.PackType)ReadByte();
        }

        private void CheckBuffer(int needLength)
        {
            if (Length < needLength)
            {
                throw new ByteBufferException("Net Buffer out of range");
            }

            if (bytesArray == null)
            {
                bytesArray = bytesList.ToArray();
            }
        }
        #endregion
    }
}
