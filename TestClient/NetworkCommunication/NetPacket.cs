﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkUtils
{
    public class NetPacket
    {
        public enum PackType : byte
        {
            Unknown,

            ServerStatus,

            Auth, // 0 - ok, 1 - wrong pass, 2 - already online

            WorldUpdate,
            
            // Gameobject
            Position,
            Color,
            Data,

            // Editor
            Add,
            Delete,
            DeleteAll,
            GenerateAll,
            ColorAll,
            GenerateRawData,
            TestMode  // 1 - start, 0 - finish
        }

        public delegate void PacketHandler(ByteBuffer buffer, object data);
        private static readonly Dictionary<PackType, PacketHandler> packetHandlers = new Dictionary<PackType, PacketHandler>();

        static NetPacket()
        {
            var names = Enum.GetNames(typeof(PackType));
            for (byte i = 0; i < names.Length; ++i)
            {
                packetHandlers.Add((PackType)i, null);
            }
        }

        public static void SetHandler(PackType packetType, PacketHandler handler)
        {
            if (handler != null)
            {
                packetHandlers[packetType] = handler;
            }
            else
            {
                throw new Exception("Packet Handler can not be null");
            }
        }

        public static void RemoveHandler(PackType packetType)
        {
            packetHandlers[packetType] = null;
        }

        public static void Handle(ByteBuffer buffer, object data)
        {
            while (buffer != null && buffer.Length > 0)
            {
                int startIndex = buffer.ReadIndex;
                try
                {
                    var packetType = buffer.ReadPackType();
                    packetHandlers[packetType]?.Invoke(buffer, data);
                }
                catch (ByteBuffer.ByteBufferException e)
                {
                    Console.WriteLine("ByteBuffer exception: " + e.Message);
                    buffer.ReadIndex = startIndex;
                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Handle pack exception: " + e.Message);
                    buffer.ReadIndex = startIndex;
                    return;
                }
            }
        }
    }
}
