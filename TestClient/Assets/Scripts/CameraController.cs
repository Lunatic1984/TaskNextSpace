﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float sensitivity = 2f;
    [SerializeField]
    private Vector3 minBounds;
    [SerializeField]
    private Vector3 maxBounds;

    void Update()
    {
        if (!NetworkManager.Instance.IsGame) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        bool b = Input.GetButton("SpeedUp");
        bool rotate = Input.GetButton("Fire2");

        transform.position += transform.forward * v * speed * Time.deltaTime * (b ? 2f : 1f);
        transform.position += transform.right * h * speed * 0.5f * Time.deltaTime * (b ? 2f : 1f);

        if (rotate)
        {
            transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * sensitivity, 0f, 0f), Space.Self);
            transform.Rotate(new Vector3(0f, Input.GetAxis("Mouse X") * sensitivity, 0f), Space.World);
        }

        Cursor.visible = !rotate;
        Cursor.lockState = rotate ? CursorLockMode.Locked : CursorLockMode.None;

        var pos = transform.position;
        if (pos.x < minBounds.x) pos.x = minBounds.x;
        if (pos.y < minBounds.y) pos.y = minBounds.y;
        if (pos.z < minBounds.z) pos.z = minBounds.z;
        if (pos.x > maxBounds.x) pos.x = maxBounds.x;
        if (pos.y > maxBounds.y) pos.y = maxBounds.y;
        if (pos.z > maxBounds.z) pos.z = maxBounds.z;

        transform.position = pos;
    }
}
