﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private InputField hostnameInput;
    [SerializeField]
    private InputField portInput;
    [SerializeField]
    private InputField nameInput;
    [SerializeField]
    private InputField passInput;
    [SerializeField]
    private GameObject conneсtingPanel;
    [SerializeField]
    private GameObject loadingPanel;
    [SerializeField]
    private GameObject toolsPanel;
    [SerializeField]
    private GameObject mainMenuPanel;

    private void Start()
    {
        NetworkManager.Instance.OnConnect += OnConnect;
        NetworkManager.Instance.OnDisconnect += OnDisconnect;
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }

    public void ButtonConnect()
    {
        if (string.IsNullOrEmpty(hostnameInput.text) ||
            string.IsNullOrEmpty(portInput.text) ||
            string.IsNullOrEmpty(nameInput.text) ||
            string.IsNullOrEmpty(passInput.text))
        {
            ScreenMessage.ShowMessage("Поля не должны быть пустыми");
            return;
        }

        conneсtingPanel.SetActive(true);

        NetworkManager.Instance.Connect(
            hostnameInput.text,
            int.Parse(portInput.text),
            nameInput.text,
            passInput.text
            );
    }

    private void OnConnect(bool success, string errorMsg)
    {
        conneсtingPanel.SetActive(false);
        if (success)
        {
            mainMenuPanel.SetActive(false);
            loadingPanel.SetActive(false);
            toolsPanel.SetActive(true);
            NetworkManager.Instance.IsGame = true;
            NetworkManager.Instance.SendUpdateWorld();                
        }
        else
        {
            ScreenMessage.ShowMessage(errorMsg);
        }
    }

    private void OnDisconnect(string errorMsg)
    {
        mainMenuPanel.SetActive(true);
        loadingPanel.SetActive(false);
        conneсtingPanel.SetActive(false);
        toolsPanel.SetActive(false);

        ScreenMessage.ShowMessage(errorMsg);        
    }
}
