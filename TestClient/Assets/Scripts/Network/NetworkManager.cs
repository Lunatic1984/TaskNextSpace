﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Net.Sockets;
using NetworkUtils;
using System;
using System.Threading;
using System.Threading.Tasks;

public class NetworkManager : MonoBehaviour
{
    [SerializeField]
    private GameObject objectPrefab;
    public GameObject ObjectPrefab { get { return objectPrefab; } }

    [SerializeField]
    private Color[] colors;
    public Color[] Colors { get { return colors; } }

    private readonly List<NetworkObject> netObjects = new List<NetworkObject>();

    private enum State
    {
        Disconnected,
        Connecting,
        Authenticating,
        Authenticated
    }

    private const float ProcessNetworkDataInterval = 0.05f;
    private const float AuthenticationTimeout = 5f;

    private NetworkConnection connection;
    private State state;

    public bool IsAuthenticated
    {
        get
        {
            return connection != null &&
                   connection.ConnectionState == NetworkConnection.State.Connected &&
                   state == State.Authenticated;
        }
    }

    public event System.Action<bool, string> OnConnect;
    public event System.Action<string> OnDisconnect;

    private volatile bool needInvokeOnConnect;
    private string lastError = string.Empty;

    private string username;
    private string userpass;

    private readonly ByteBuffer receiveBuffer = new ByteBuffer();

    public bool IsGame { get; set; }

    public event System.Action<bool> TestModeEvent;

    public static NetworkManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        NetPacket.SetHandler(NetPacket.PackType.Auth, HandleAuth);
        NetPacket.SetHandler(NetPacket.PackType.Position, HandleSerializePosition);
        NetPacket.SetHandler(NetPacket.PackType.Color, HandleSerializeColor);
        NetPacket.SetHandler(NetPacket.PackType.Data, HandleSerializeData);
        NetPacket.SetHandler(NetPacket.PackType.WorldUpdate, HandleUpdateWorld);
        NetPacket.SetHandler(NetPacket.PackType.Delete, HandleDeleteObject);
        NetPacket.SetHandler(NetPacket.PackType.TestMode, HandleTestMode);
    }

    private void Update()
    {
        if (state == State.Disconnected && needInvokeOnConnect)
        {
            OnConnect?.Invoke(false, lastError);
            needInvokeOnConnect = false;
        }

        if (connection == null) return;

        CheckConnection();
        if (state == State.Authenticated)
        {
            if (needInvokeOnConnect)
            {
                OnConnect?.Invoke(true, "");
                needInvokeOnConnect = false;
            }

            lock (netObjects)
            {
                for (int i = 0; i < netObjects.Count; ++i)
                {
                    netObjects[i].Update(Time.deltaTime);
                }
                netObjects.RemoveAll(p => p.IsDestroyed);

                name = $"NetworkManager ({netObjects.Count})";
            }
        }        
    }

    private void ProcessNetworkData()
    {
        while (state == State.Authenticating || state == State.Authenticated)
        {
            var buf = connection.GetReceiveData();
            if (buf != null && buf.Length > 0)
            {               
                receiveBuffer.Append(buf);             
                NetPacket.Handle(receiveBuffer, null);                
            }
            Thread.Sleep(10);
        }
    }

    private IEnumerator AuthenticationTimeoutRoutine()
    {
        yield return new WaitForSecondsRealtime(AuthenticationTimeout);
        if (state == State.Authenticating)
        {
            state = State.Disconnected;
            connection.Disconnect();
            connection = null;
            OnConnect?.Invoke(false, "Authentication timeout");
        }
    }

    private void CheckConnection()
    {
        // Разрыв соединения?
        if ((state == State.Authenticated || state == State.Authenticating) && connection.ConnectionState != NetworkConnection.State.Connected)
        {
            state = State.Disconnected;
            OnDisconnect?.Invoke(connection.GetConnectionError());
            connection = null;
            lock(netObjects)
            {
                foreach(var obj in netObjects)
                {
                    obj.Destroy();
                }
            }
            return;
        }

        // Если происходит подключение
        if (state == State.Connecting)
        {
            // Если подключение прервалось
            if (connection.ConnectionState == NetworkConnection.State.Disconnected)
            {
                state = State.Disconnected;
                OnConnect?.Invoke(false, connection.GetConnectionError());
                Debug.Log($"<color=red><b>Connection error: {connection.GetConnectionError()}</b></color>");
                connection = null;
                return;
            }
            // Если подключились успешно
            if (connection.ConnectionState == NetworkConnection.State.Connected)
            {
                state = State.Authenticating;
                var buf = new ByteBuffer();
                buf.WritePackType(NetPacket.PackType.Auth);
                buf.WriteString(username);
                buf.WriteInteger(userpass.GetHashCode());
                connection.SendData(buf);
                StartCoroutine(AuthenticationTimeoutRoutine());
                Task.Run(() => { ProcessNetworkData(); });
            }
            // все еще подключаемся, ждем
            return;
        }
    }

    private void OnDestroy()
    {
        Disconnect();
    }

    public void Connect(string hostname, int port, string name, string password)
    {
        if (connection == null || connection.ConnectionState == NetworkConnection.State.Disconnected)
        {
            connection = new NetworkConnection();
            connection.Connect(hostname, port);
            username = name;
            userpass = password;
            state = State.Connecting;
            receiveBuffer.Clear();
        }
    }

    public void Disconnect()
    {
        state = State.Disconnected;
        if (connection != null)
        {
            connection.Disconnect();
            connection = null;
        }
    }

    private NetworkObject FindNetworkObject(Guid id)
    {
        var result = netObjects.FirstOrDefault(p => p.Id == id);
        if (result == null)
        {
            result = new NetworkObject(id);
            netObjects.Add(result);
        }
        return result;
    }

    public NetworkObject[] GetNetObjects()
    {
        lock (netObjects)
        {
            return netObjects.ToArray();
        }
    }

    #region SEND COMMANDS

    public void SendUpdateWorld()
    {
        var buf = new ByteBuffer();
        buf.WritePackType(NetPacket.PackType.WorldUpdate);

        connection.SendData(buf);
    }

    public void SendDeleteObjects(Guid[] ids)
    {
        var buf = new ByteBuffer();
        foreach (var id in ids)
        {
            buf.WritePackType(NetPacket.PackType.Delete);
            buf.WriteGuid(id);
        }
        connection.SendData(buf);
    }

    public void SendDeleteAllObjects()
    {
        var buf = new ByteBuffer();
        buf.WritePackType(NetPacket.PackType.DeleteAll);
        connection.SendData(buf);
    }

    public void SendGenerateAllObjects(int count)
    {
        var buf = new ByteBuffer();
        buf.WritePackType(NetPacket.PackType.GenerateAll);
        buf.WriteInteger(count);
        connection.SendData(buf);
    }

    public void SendGenerateRawDataObjects(Guid[] ids, int count)
    {
        var buf = new ByteBuffer();
        foreach (var id in ids)
        {
            buf.WritePackType(NetPacket.PackType.GenerateRawData);
            buf.WriteGuid(id);
            buf.WriteInteger(count);
        }
        connection.SendData(buf);
    }

    public void SendColorAll(byte color)
    {
        var buf = new ByteBuffer();
        buf.WritePackType(NetPacket.PackType.ColorAll);
        buf.WriteByte(color);
        connection.SendData(buf);
    }

    public void SendColorObjects(Guid[] ids, byte color)
    {
        var buf = new ByteBuffer();
        foreach (var id in ids)
        {
            buf.WritePackType(NetPacket.PackType.Color);
            buf.WriteGuid(id);
            buf.WriteByte(color);
        }
        connection.SendData(buf);
    }

    public void SendAddObject(Vector3 pos, byte color)
    {
        var buf = new ByteBuffer();

        buf.WritePackType(NetPacket.PackType.Add);
        buf.WriteFloat(pos.x);
        buf.WriteFloat(pos.y);
        buf.WriteFloat(pos.z);
        buf.WriteByte(color);

        connection.SendData(buf);
    }

    public void SendStartTest(int ballsCount, int intensity, byte changes)
    {
        var buf = new ByteBuffer();

        buf.WritePackType(NetPacket.PackType.TestMode);
        buf.WriteByte(1); // 1 - start
        buf.WriteInteger(ballsCount);
        buf.WriteInteger(intensity);
        buf.WriteByte(changes);

        connection.SendData(buf);
    }

    public void SendStopTest()
    {
        var buf = new ByteBuffer();

        buf.WritePackType(NetPacket.PackType.TestMode);
        buf.WriteByte(0); // 0 - stop

        connection.SendData(buf);
    }

    #endregion

    #region HANDLE SERVER MESSAGES

    private void HandleAuth(ByteBuffer buffer, object data)
    {
        var result = buffer.ReadByte();
        if (state == State.Authenticating)
        {
            switch (result)
            {
                case 0:
                    state = State.Authenticated;
                    lastError = string.Empty;
                    needInvokeOnConnect = true;
                    break;
                case 1:
                    connection.Disconnect();
                    connection = null;
                    state = State.Disconnected;
                    lastError = "Wrong name or password";
                    needInvokeOnConnect = true;
                    break;
                case 2:
                    connection.Disconnect();
                    connection = null;
                    state = State.Disconnected;
                    lastError = "User already in game";
                    needInvokeOnConnect = true;
                    break;
                default:
                    connection.Disconnect();
                    connection = null;
                    state = State.Disconnected;
                    lastError = "Unknown error";
                    needInvokeOnConnect = true;
                    break;
            }
        }
    }

    private void HandleSerializePosition(ByteBuffer buf, object client)
    {
        var id = buf.ReadGuid();
        float x = buf.ReadFloat();
        float y = buf.ReadFloat();
        float z = buf.ReadFloat();

        lock (netObjects)
        {
            var obj = FindNetworkObject(id);
            obj.Position = new Vector3(x, y, z);
        }
    }

    private void HandleSerializeColor(ByteBuffer buf, object client)
    {
        var id = buf.ReadGuid();
        byte color = buf.ReadByte();

        lock (netObjects)
        {
            var obj = FindNetworkObject(id);
            obj.Color = color;
        }
    }

    private void HandleSerializeData(ByteBuffer buf, object client)
    {
        var id = buf.ReadGuid();
        int count = buf.ReadInteger();
        byte[] bytes = buf.ReadBytes(count);

        lock (netObjects)
        {
            var obj = FindNetworkObject(id);
            obj.Data = bytes;
        }
    }

    private void HandleUpdateWorld(ByteBuffer buf, object client)
    {
        //worldLoadedEvent?.Invoke();
    }

    private void HandleDeleteObject(ByteBuffer buf, object client)
    {
        var id = buf.ReadGuid();
        lock (netObjects)
        {
            var obj = netObjects.FirstOrDefault(p => p.Id == id);
            if (obj != null)
            {
                obj.Destroy();
            }
        }
    }

    private void HandleTestMode(ByteBuffer buf, object client)
    {
        var started = buf.ReadByte();

        TestModeEvent?.Invoke(started > 0);
    }

    #endregion
}
