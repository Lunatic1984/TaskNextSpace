﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;
using NetworkUtils;
using System.Threading;

public class NetworkConnection
{
    public class NetworkConnectionException : Exception
    {
        public NetworkConnectionException(string message) : base(message) { }
    }

    public enum State
    {
        Disconnected,
        Connecting,
        Connected
    }

    private volatile State state;
    public State ConnectionState { get { return state; } }

    private TcpClient tcpClient;
    private NetworkStream stream;
    private readonly ByteBuffer receiveBuffer = new ByteBuffer();
    private readonly ByteBuffer sendBuffer = new ByteBuffer();

    private string lastError = string.Empty;

    private const int NetworkDelay = 10;
    private const int ReceiveBufferSize = 1024 * 1024;

    public string GetConnectionError()
    {
        lock (lastError)
        {
            return lastError;
        }
    }

    public void Connect(string hostname, int port)
    {
        if (state != State.Disconnected) throw new NetworkConnectionException("Already connecting or connected");
        if (string.IsNullOrEmpty(hostname)) throw new ArgumentException("Hostname null or empty");

        state = State.Connecting;

        Task.Run(() =>
        {
            try
            {
                tcpClient = new TcpClient(hostname, port);
                stream = tcpClient.GetStream();
                state = State.Connected;
                Task.Run(() => RecieveThread());
                Task.Run(() => SendThread());
            }
            catch (Exception e)
            {
                lock (lastError)
                {
                    lastError = e.Message;
                }
                tcpClient = null;
                state = State.Disconnected;
                lastError = e.Message;
                return;
            }
        });
    }

    public void Disconnect()
    {
        state = State.Disconnected;
        if (tcpClient != null)
        {
            tcpClient.Close();
            tcpClient = null;
            stream = null;
        }
    }

    public void SendData(ByteBuffer buffer)
    {
        if (buffer.Length > 0)
        {
            Task.Run(() =>
            {
                lock (sendBuffer)
                {
                    sendBuffer.Append(buffer);
                }
            });
        }
    }

    public ByteBuffer GetReceiveData()
    {
        ByteBuffer result = null;
        lock (receiveBuffer)
        {
            result = new ByteBuffer(receiveBuffer);
            receiveBuffer.Clear();
        }
        return result;
    }

    private void RecieveThread()
    {
        var byteArray = new byte[ReceiveBufferSize];
        try
        {
            while (state == State.Connected)
            {
                do
                {
                    int sz = stream.Read(byteArray, 0, ReceiveBufferSize);
                    if (sz > 0)
                    {
                        lock (receiveBuffer)
                        {
                            receiveBuffer.FromBytes(byteArray, sz);
                        }
                    }
                    else
                    {
                        Disconnect();
                    }
                }
                while (stream.DataAvailable);

                Thread.Sleep(NetworkDelay);
            }
        }
        catch (Exception e)
        {
            Disconnect();
            lastError = e.Message;
            Debug.Log($"<color=red>NetworkConnection.ReceiveThread: {e.Message}</color>");
        }
    }

    private void SendThread()
    {
        try
        {
            while (state == State.Connected)
            {
                byte[] bytes = null;

                lock (sendBuffer)
                {
                    bytes = sendBuffer.ToBytes();
                    sendBuffer.Clear();
                }

                if (bytes != null && bytes.Length > 0)
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                Thread.Sleep(NetworkDelay);
            }
        }
        catch (Exception e)
        {
            Disconnect();
            lastError = e.Message;
            Debug.Log($"<color=red>NetworkConnection.SendThread: {e.Message}</color>");
        }
    }
}
