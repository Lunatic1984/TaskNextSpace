﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditManager : MonoBehaviour
{
    public enum Mode
    {
        None = -1,
        Add,
        Delete,
        Color,
        Data
    }

    private Camera cam;

    private Mode editMode = Mode.Delete;
    private byte editColor = 0;
    private bool addAvailable;

    [SerializeField]
    private GameObject previewPrefab;
    private GameObject previewObject;
    [SerializeField]
    private GameObject brushImage;
    [SerializeField]
    private RectTransform canvasTransform;

    [SerializeField]
    private InputField generateAllCountInputField;
    [SerializeField]
    private InputField rawDataCountInputField;
    [SerializeField]
    private Slider bruchSizeSlider;
        
    private float brushRadius = 1;

    [SerializeField]
    private GameObject[] selectColorImages;
    [SerializeField]
    private GameObject[] selectModeImages;

    [Header("Test Mode")]
    [SerializeField]
    private InputField ballsCountInputField;
    [SerializeField]
    private InputField intensityInputField;
    [SerializeField]
    private Toggle ballsToggle;
    [SerializeField]
    private Toggle colorToggle;
    [SerializeField]
    private Toggle dataToggle;
    [Space]
    [SerializeField]
    private GameObject startTestButton;
    [SerializeField]
    private GameObject stopTestButton;

    private volatile bool isTestMode;

    private void Start()
    {
        cam = Camera.main;
        ButtonSetColor(0);
        ButtonSetMode(2);

        brushRadius = bruchSizeSlider.value * Screen.height * 0.5f;

        NetworkManager.Instance.TestModeEvent += (started) => 
        {
            isTestMode = started;
        };
    }

    private void Update()
    {
        stopTestButton.SetActive(isTestMode);
        brushImage.SetActive(false);
        if (!NetworkManager.Instance.IsGame || EventSystem.current.IsPointerOverGameObject()) return;

        if (editMode == Mode.None || editMode == Mode.Add || Input.GetButton("Fire2")) return;

        brushImage.SetActive(true);
        var rect = brushImage.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(brushRadius * 2f, brushRadius * 2f) / canvasTransform.localScale.y;
        rect.position = Input.mousePosition;

        var color = Color.gray;
        if (editMode == Mode.Color)
        {
            switch (editColor)
            {
                case 0:
                    color = Color.yellow;
                    break;
                case 1:
                    color = Color.cyan;
                    break;
                case 2:
                    color = Color.blue;
                    break;
            }
        }
        else if (editMode == Mode.Data)
        {
            color = Color.white;
        }
        color.a = 0.5f;
        brushImage.GetComponent<Image>().color = color;

        if (Input.GetButtonDown("Fire1"))
        {
            if (editMode == Mode.Add)
            {
                if (addAvailable)
                {
                    NetworkManager.Instance.SendAddObject(previewObject.transform.position, editColor);
                }
                return;
            }

            var ids = GetObjectIdsInScreenRadius(brushRadius);
            switch (editMode)
            {
                case Mode.Delete:
                    NetworkManager.Instance.SendDeleteObjects(ids);
                    break;
                case Mode.Color:
                    NetworkManager.Instance.SendColorObjects(ids, editColor);
                    break;
                case Mode.Data:
                    NetworkManager.Instance.SendGenerateRawDataObjects(ids, int.Parse(rawDataCountInputField.text));
                    break;
            }
        }
    }

    private Guid[] GetObjectIdsInScreenRadius(float radius)
    {
        var cam = Camera.main;
        var idsList = new List<Guid>();
                
        var objs = NetworkManager.Instance.GetNetObjects();
        
        foreach (var obj in objs)
        {
            if (cam.transform.InverseTransformPoint(obj.Position).z > 0f)
            {
                var screenPosCenter = cam.WorldToScreenPoint(obj.Position);
                var screenPosEdge = cam.WorldToScreenPoint(obj.Position + cam.transform.right * 0.5f);
                var screenRadius = Vector3.Magnitude(screenPosCenter - screenPosEdge);               

                if (Vector3.Magnitude(screenPosCenter - Input.mousePosition) <
                    (radius + screenRadius))
                {
                    idsList.Add(obj.Id);
                }
            }
        }

        return idsList.ToArray();
    }

    private void FixedUpdate()
    {
        PreviewAdd();
    }

    private void PreviewAdd()
    {
        if (!NetworkManager.Instance.IsGame || editMode != Mode.Add || EventSystem.current.IsPointerOverGameObject())
        {
            if (previewObject != null && previewObject.activeInHierarchy)
            {
                previewObject.SetActive(false);
                addAvailable = false;
            }
            return;
        }

        if (previewObject == null)
        {
            previewObject = Instantiate(previewPrefab);
        }

        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 100f, LayerMask.GetMask("Ground")))
        {
            previewObject.SetActive(true);
            previewObject.transform.position = hit.point + Vector3.up * 0.5f;
            if (Physics.OverlapSphere(previewObject.transform.position, 0.5f, ~LayerMask.GetMask("Ground")).Length == 0)
            {
                previewObject.GetComponent<Renderer>().material.color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
                addAvailable = true;
            }
            else
            {
                previewObject.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
                addAvailable = false;
            }
        }
        else
        {
            previewObject.SetActive(false);
            addAvailable = false;
        }
    }

    public void ButtonSetMode(int mode)
    {
        if (editMode == (Mode)mode)
        {
            editMode = Mode.None;
        }
        else
        {
            editMode = (Mode)mode;
        }
        for (int i = 0; i < selectModeImages.Length; ++i)
        {
            selectModeImages[i].SetActive((int)editMode == i);
        }
    }

    public void ButtonSetColor(int color)
    {
        editColor = (byte)color;
        for (int i = 0; i < selectColorImages.Length; ++i)
        {
            selectColorImages[i].SetActive(editColor == i);
        }
    }

    public void ButtonDeleteAll()
    {
        NetworkManager.Instance.SendDeleteAllObjects();
    }

    public void ButtonGenerateAll()
    {
        NetworkManager.Instance.SendGenerateAllObjects(int.Parse(generateAllCountInputField.text));
    }

    public void ButtonColorAll()
    {
        NetworkManager.Instance.SendColorAll(editColor);
    }

    public void ButtonStartTestMode()
    {
        int ballsCount = int.Parse(ballsCountInputField.text);
        int intensity = int.Parse(intensityInputField.text);
        bool move = ballsToggle.isOn;
        bool color = colorToggle.isOn;
        bool data = dataToggle.isOn;

        byte changes = 0;
        if (move) changes |= 1 << 1;
        if (color) changes |= 1 << 2;
        if (data) changes |= 1 << 3;
        
        NetworkManager.Instance.SendStartTest(ballsCount, intensity, changes);
    }

    public void ButtonStopTestMode()
    {
        NetworkManager.Instance.SendStopTest();
    }

    public void OnChangeBrushValue(float value)
    {
        brushRadius = bruchSizeSlider.value * Screen.height * 0.5f;
    }
}
