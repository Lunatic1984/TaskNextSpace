﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenMessage : MonoBehaviour
{
    [SerializeField]
    private GameObject messageObject;
    [SerializeField]
    private Text textObject;

    private static ScreenMessage instance;

    private void Awake()
    {
        instance = this;
    }
    
    private IEnumerator ShowRoutine(float showTime)
    {
        messageObject.SetActive(true);
        yield return new WaitForSecondsRealtime(showTime);
        messageObject.SetActive(false);
    }

    public static void ShowMessage(string text, float showTime = 3f)
    {
        if (instance != null)
        {
            instance.textObject.text = text;
            instance.StopAllCoroutines();
            instance.StartCoroutine(instance.ShowRoutine(showTime));
        }
    }
}
