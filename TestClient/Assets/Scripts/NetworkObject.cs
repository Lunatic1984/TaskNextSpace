﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkObject
{
    public Guid Id { get; private set; }

    private Vector3 position;
    public Vector3 Position
    {
        set
        {
            if (position != value)
            {
                isUpdated = false;
                position = value;
            }
        }
        get
        {
            return position;
        }
    }

    private byte color;
    public byte Color
    {
        set
        {
            if (color != value)
            {
                isUpdated = false;
                color = (byte)Mathf.Clamp(value, 0, 3);
            }
        }
    }

    private byte[] data;
    public byte[] Data
    {
        set
        {            
            data = new byte[value.Length];
            value.CopyTo(data, 0);
        }
    }

    private volatile bool isUpdated;

    public bool IsDestroyed { get; private set; }

    private GameObject gameObject;


    public NetworkObject(Guid id)
    {
        Id = id;
        isUpdated = false;
    }

    public void Update(float deltaTime)
    {
        if (IsDestroyed)
        {
            if (gameObject != null)
            {
                GameObject.Destroy(gameObject);
                gameObject = null;
            }
            return;
        }

        if (!isUpdated)
        {
            if (gameObject == null)
            {
                gameObject = GameObject.Instantiate(NetworkManager.Instance.ObjectPrefab);
                gameObject.transform.parent = NetworkManager.Instance.transform;
                gameObject.transform.position = position;
                gameObject.transform.name = "Object - " + Id;
            }

            gameObject.GetComponent<Renderer>().material.color = NetworkManager.Instance.Colors[color];

            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, position, deltaTime * 5f);
        }
    }

    public void Destroy()
    {
        IsDestroyed = true;        
    }   
}
