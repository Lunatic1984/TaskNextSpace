﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using NetworkUtils;

namespace TestServer
{
    class GameServer : Server
    {
        public const int MainThreadDelay = 30;

        private int port = 7777;

        private World world;

        private bool isTestMode;
        private bool isTestModeSynchronized = true;
        private int testObjectsCount;
        private int testIntensity;
        private bool testPosition;
        private bool testColor;
        private bool testData;

        private readonly List<User> users = new List<User>();

        private readonly ChangeHistory changeHistory = new ChangeHistory();

        private const long MaxAuthTime = 5000;

        public GameServer(int port, bool sendStatus = false) : base()
        {
            this.port = port;

            NetPacket.SetHandler(NetPacket.PackType.Auth, HandleAuth);
            NetPacket.SetHandler(NetPacket.PackType.WorldUpdate, HandleWorldUpdate);
            NetPacket.SetHandler(NetPacket.PackType.Add, HandleAdd);
            NetPacket.SetHandler(NetPacket.PackType.GenerateAll, HandleGenerateAll);
            NetPacket.SetHandler(NetPacket.PackType.Delete, HandleDelete);
            NetPacket.SetHandler(NetPacket.PackType.DeleteAll, HandleDeleteAll);
            NetPacket.SetHandler(NetPacket.PackType.Color, HandleColor);
            NetPacket.SetHandler(NetPacket.PackType.ColorAll, HandleColorAll);
            NetPacket.SetHandler(NetPacket.PackType.GenerateRawData, HandleGenerateRawData);
            NetPacket.SetHandler(NetPacket.PackType.TestMode, HandleTestMode);

            world = new World();
        }

        public void Run(int port = 7777)
        {
            this.port = port;

            if (!Start(port))
            {
                return;
            }

            var testTime = new TimeSpan(DateTime.Now.Ticks);

            bool isRunning = true;
            while (isRunning)
            {
                if (isTestMode && (new TimeSpan(DateTime.Now.Ticks) - testTime).TotalMilliseconds > testIntensity)
                {
                    world.TestModeUpdate();
                    testTime = new TimeSpan(DateTime.Now.Ticks);
                }

                world.Update();

                ProcessClients(network.GetNetClients());

                Thread.Sleep(MainThreadDelay);
            }

            Stop();
        }

        private void ProcessClients(NetClient[] clients)
        {
            foreach (var client in clients)
            {
                if (client.IsAuthenticated)
                {
                    if (!isTestModeSynchronized || client.User.NeedWorldUpdate)
                    {
                        var buf = new ByteBuffer();
                        buf.WritePackType(NetPacket.PackType.TestMode);
                        buf.WriteByte(isTestMode ? (byte)1 : (byte)0);
                        client.Send(buf);
                    }

                    if (client.User.NeedWorldUpdate)
                    {
                        client.User.NeedWorldUpdate = false;
                        client.Send(world.GetSerializeAllData());

                        var buf = new ByteBuffer();
                        buf.WritePackType(NetPacket.PackType.WorldUpdate);
                        client.Send(buf);
                    }
                    else
                    {
                        client.Send(world.GetSerializeData());
                    }
                }
                else
                {
                    if (DateTime.Now.Ticks - client.ConnectTick > MaxAuthTime * TimeSpan.TicksPerMillisecond)
                    {
                        client.Disconnect();
                    }
                }
            }

            isTestModeSynchronized = true;
        }

        #region HANDLE MESSAGES

        private void HandleAuth(ByteBuffer buf, object client)
        {
            NetClient c = (NetClient)client;
            var name = buf.ReadString();
            var pass = buf.ReadInteger();

            if (c.IsAuthenticated) return;

            Task.Run(() =>
            {
                User user = null;
                lock (users)
                {
                    user = users.FirstOrDefault(p => p.Name.Equals(name));
                }
                // Пользователь найден
                if (user != null)
                {
                    byte result = user.Password != pass ? (byte)1 : (user.IsOnline ? (byte)2 : (byte)0);
                    buf = new ByteBuffer();
                    buf.WritePackType(NetPacket.PackType.Auth);
                    buf.WriteByte(result);
                    c.Send(buf);

                    switch (result)
                    {
                        case 0: // Авторизация успешна
                            user.IsOnline = true;
                            c.Auth(user);
                            Console.WriteLine($"User {user.Name} authorized");
                            break;
                        case 1: // Неверный пароль
                            Console.WriteLine($"User {user.Name} wrong password");
                            c.Disconnect();
                            break;
                        default:
                            Console.WriteLine($"User {user.Name} already online");
                            c.Disconnect();
                            break;
                    }

                }
                else
                {
                    // Создание нового пользователя
                    byte result = 0;
                    buf = new ByteBuffer();
                    buf.WritePackType(NetPacket.PackType.Auth);
                    buf.WriteByte(result);
                    c.Send(buf);

                    user = new User
                    {
                        Name = name,
                        Password = pass,
                        IsOnline = true
                    };
                    c.Auth(user);
                    Console.WriteLine($"User {user.Name} created and authorized");

                    lock (users)
                    {
                        users.Add(user);
                    }
                }
            });
        }

        private void HandleWorldUpdate(ByteBuffer buf, object client)
        {
            ((NetClient)client).User.NeedWorldUpdate = true;
        }

        private void HandleDelete(ByteBuffer buf, object client)
        {
            var id = buf.ReadGuid();
            world.DeleteObject(id);

            changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
        }

        private void HandleDeleteAll(ByteBuffer buf, object client)
        {
            world.DeleteAll();

            changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
        }

        private void HandleColor(ByteBuffer buf, object client)
        {
            var id = buf.ReadGuid();
            var color = buf.ReadByte();
            var go = world.FindObject(id);
            if (go != null)
            {
                go.Color = color;

                changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
            }
        }

        private void HandleAdd(ByteBuffer buf, object client)
        {
            float x = buf.ReadFloat();
            float y = buf.ReadFloat();
            float z = buf.ReadFloat();
            var color = buf.ReadByte();

            var go = world.AddObject(new Vector3(x, 0f, z), color);

            changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
        }

        private void HandleGenerateAll(ByteBuffer buf, object client)
        {
            int count = buf.ReadInteger();
            world.DeleteAll();
            world.GenerateObjects(count);

            changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
        }

        private void HandleColorAll(ByteBuffer buf, object client)
        {
            var color = buf.ReadByte();
            world.ColorAll(color);

            changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
        }

        private void HandleGenerateRawData(ByteBuffer buf, object client)
        {
            var id = buf.ReadGuid();
            int count = buf.ReadInteger();
            var go = world.FindObject(id);
            if (go != null)
            {
                go.GenerateRandomData(count);

                changeHistory.Commit(world.GetAllObjects(), ((NetClient)client).User.Id);
            }
        }

        private void HandleTestMode(ByteBuffer buf, object client)
        {
            var started = buf.ReadByte();
            if (started > 0)
            {
                testObjectsCount = buf.ReadInteger();
                testIntensity = buf.ReadInteger();

                var changes = buf.ReadByte();
                testPosition = (changes & 1 << 1) != 0;
                testColor = (changes & 1 << 2) != 0;
                testData = (changes & 1 << 3) != 0;

                world.InitTestMode(testObjectsCount, testPosition, testColor, testData);

                Console.WriteLine($"Start testmode: pos = {testPosition}, color = {testColor}, data = {testData}");
            }
            else
            {
                Console.WriteLine("Stop testmode");
            }

            isTestMode = started > 0;
            isTestModeSynchronized = false;
        }

        #endregion

    }
}
