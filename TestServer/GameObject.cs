﻿using NetworkUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    class GameObject
    {
        public Guid Id { get; private set; }

        private Vector3 position;
        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                IsSynchronizedPosition = false;
            }
        }
        private byte color;
        public byte Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                IsSynchronizedColor = false;
            }
        }
        public byte[] Data { get; private set; }
        public bool IsSynchronizedPosition { get; private set; }
        public bool IsSynchronizedColor { get; private set; }
        public bool IsSynchronizedData { get; private set; }
        public bool IsSynchronized
        {
            get
            {
                return IsSynchronizedPosition && IsSynchronizedColor && IsSynchronizedData && !IsDeleted;
            }
        }

        public bool IsDeleted { get; set; }

        public const int MinDataSize = 100;
        public const int MaxDataSize = 5 * 1024;

        public GameObject(Guid id, Vector3 pos, byte color = 0)
        {
            Id = id;
            Position = pos;
            Color = color;
            GenerateRandomData(new Random().Next(MaxDataSize - MinDataSize) + MinDataSize);
        }

        public GameObject(GameObject copy)
        {
            Id = copy.Id;
            position = copy.position;
            color = copy.color;
            if (copy.Data != null)
            {
                Data = new byte[copy.Data.Length];
                copy.Data.CopyTo(Data, 0);
            }
            IsDeleted = copy.IsDeleted;
        }

        public void GenerateRandomData(int size)
        {
            size = Math.Max(MinDataSize, size);
            size = Math.Min(size, MaxDataSize);

            Data = new byte[size];
            new Random().NextBytes(Data);

            IsSynchronizedData = false;
        }

        private byte[] GetPositionBytes()
        {
            var buf = new ByteBuffer();
            buf.WritePackType(NetPacket.PackType.Position);
            buf.WriteGuid(Id);
            buf.WriteFloat(position.X);
            buf.WriteFloat(position.Y);
            buf.WriteFloat(position.Z);
            IsSynchronizedPosition = true;

            return buf.ToBytes();
        }

        private byte[] GetColorBytes()
        {
            var buf = new ByteBuffer();
            buf.WritePackType(NetPacket.PackType.Color);
            buf.WriteGuid(Id);
            buf.WriteByte(color);
            IsSynchronizedColor = true;
            
            return buf.ToBytes();
        }

        private byte[] GetDataBytes()
        {
            var buf = new ByteBuffer();
            buf.WritePackType(NetPacket.PackType.Data);
            buf.WriteGuid(Id);
            buf.WriteInteger(Data.Length);
            buf.WriteBytes(Data);
            IsSynchronizedData = true;

            return buf.ToBytes();
        }

        public byte[] GetSerializeBytes()
        {
            var buf = new ByteBuffer();

            if (!IsSynchronizedPosition)
            {
                buf.WriteBytes(GetPositionBytes());
            }
            if (!IsSynchronizedColor)
            {
                buf.WriteBytes(GetColorBytes());
            }
            if (!IsSynchronizedData)
            {
                buf.WriteBytes(GetDataBytes());
            }

            return buf.ToBytes();
        }

        public byte[] GetSerializeAllBytes()
        {
            var buf = new ByteBuffer();

            buf.WriteBytes(GetPositionBytes());
            buf.WriteBytes(GetColorBytes());
            buf.WriteBytes(GetDataBytes());

            return buf.ToBytes();
        }
    }
}
