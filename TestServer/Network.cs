﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using NetworkUtils;

namespace TestServer
{
    class Network
    {
        private TcpListener tcpListener;

        private readonly List<NetClient> netClients = new List<NetClient>();

        public int Port { get; private set; }

        private const int ListenTickDelay = 10;
        private Timer listenTimer;

        public Network(int port)
        {
            Port = port;
        }

        public bool Start()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, Port);
                tcpListener.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Network: {e.Message}");
                return false;
            }

            listenTimer = new Timer(ListenTick, null, ListenTickDelay, ListenTickDelay);

            Console.WriteLine($"Network: Start listen port: {Port}");
            return true;
        }

        public void Stop()
        {
            if (listenTimer != null)
            {
                listenTimer.Dispose();
                listenTimer = null;
            }
            if (tcpListener != null)
            {
                tcpListener.Stop();
                tcpListener = null;
            }
            lock (netClients)
            {
                netClients.Clear();
            }

            Console.WriteLine($"Network: Stop listen port: {Port}");
        }

        public NetClient[] GetNetClients()
        {
            lock (netClients)
            {
                return netClients.ToArray();
            }
        }

        public NetClient[] RemoveDisconnected()
        {
            lock (netClients)
            {
                var result = netClients.Where(p => !p.IsConnected).ToArray();
                netClients.RemoveAll(p => !p.IsConnected);
                return result;
            }
        }

        private void ListenTick(object state)
        {
            try
            {
                while (tcpListener != null && tcpListener.Pending())
                {
                    var tcpClient = tcpListener.AcceptTcpClient();
                    if (tcpClient != null)
                    {
                        var netClient = new NetClient(tcpClient);
                        int count = 0;
                        lock (netClients)
                        {
                            netClients.Add(netClient);
                            count = netClients.Count;
                        }
                        Console.WriteLine($"Network: Client connected ({count})");
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine($"Network: Listen exception: {e.Message}");
            }

        }
    }
}
