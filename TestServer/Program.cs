﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    class Program
    {
        private const int defPort = 7777;

        static void Main(string[] args)
        {
            int port = defPort;
            bool status = false;
            if (args.Length > 0)
            {
                if (!int.TryParse(args[0], out port))
                {
                    port = defPort;
                }
                status = args.FirstOrDefault(p => p.Equals("-s")) != null;
            }
            var server = new GameServer(port, status);
            server.Run();
        }
    }
}
