﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetworkUtils;

namespace TestServer
{
    class NetClient
    {
        private TcpClient tcpClient;
        private NetworkStream stream;

        private bool isConnected;
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
        }

        private User user;
        public User User { get { return user; } }

        public bool IsAuthenticated { get { return user != null; } }

        public long ConnectTick { get; private set; }

        private readonly ByteBuffer receiveBuffer = new ByteBuffer();
        private readonly ByteBuffer sendBuffer = new ByteBuffer();

        private const int BufferSize = 1024;
        private readonly byte[] byteArray = new byte[BufferSize];

        private const int NetClientThreadDelay = 10;
        private const int EnterWriteLockTimeout = 2;

        public NetClient(TcpClient tcpClient)
        {
            this.tcpClient = tcpClient;
            stream = tcpClient.GetStream();
            isConnected = true;

            new Thread(ReceiveThread).Start();
            new Thread(SendThread).Start();

            ConnectTick = DateTime.Now.Ticks;
        }

        ~NetClient()
        {
            if (tcpClient != null)
            {
                tcpClient.Close();
            }
        }

        private void SendThread()
        {
            try
            {
                while (IsConnected)
                {
                    SendNetworkData();

                    Thread.Sleep(NetClientThreadDelay);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"NetClient SendThread Exception: {e.Message}");
            }
            finally
            {
                isConnected = false;
                if (user != null) user.IsOnline = false;
            }
        }

        private void ReceiveThread()
        {
            try
            {
                while (IsConnected)
                {
                    ReceiveNetworkData();

                    Thread.Sleep(NetClientThreadDelay);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"NetClient ReceiveThread Exception: {e.Message}");
            }
            finally
            {
                isConnected = false;
                if (user != null) user.IsOnline = false;
            }
        }

        private void ReceiveNetworkData()
        {
            do
            {
                int sz = stream.Read(byteArray, 0, BufferSize);
                if (sz > 0)
                {
                    lock (receiveBuffer)
                    {
                        receiveBuffer.FromBytes(byteArray, sz);
                    }
                }
                else
                {
                    isConnected = false;
                }
            }
            while (stream.DataAvailable);
        }

        private void SendNetworkData()
        {
            if (!isConnected) return;
            try
            {
                byte[] bytes = null;

                lock (sendBuffer)
                {
                    bytes = sendBuffer.ToBytes();
                    sendBuffer.Clear();
                }

                if (bytes != null && bytes.Length > 0)
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Send(ByteBuffer buf)
        {
            if (!isConnected) return;

            byte[] bytes = buf.ToBytes();
            if (bytes.Length > 0)
            {
                Task.Run(() =>
                {
                    lock (sendBuffer)
                    {
                        sendBuffer.FromBytes(bytes, bytes.Length);
                    }
                });
            }
        }

        public ByteBuffer Receive()
        {
            ByteBuffer result = null;
            lock (receiveBuffer)
            {
                if (receiveBuffer.Length > 0)
                {
                    result = new ByteBuffer(receiveBuffer);
                    receiveBuffer.Clear();
                }
            }
            return result;
        }

        public void ReturnToReceiveBuffer(ByteBuffer buf)
        {
            lock (receiveBuffer)
            {
                receiveBuffer.Clear();
                receiveBuffer.Append(buf);
            }
        }

        public void Disconnect()
        {
            // Задержка перед отключением, чтобы сервер успел отправить данные клиенту
            Task.Run(() =>
            {
                Thread.Sleep(50);
                isConnected = false;
                if (user != null)
                {
                    user.IsOnline = false;
                }

                //if (tcpClient != null)
                //{
                //    tcpClient.Close();
                //    tcpClient = null;
                //}
            });
        }

        public void Auth(User user)
        {
            this.user = user;
            user.NetClient = this;
        }
    }
}
