﻿using NetworkUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestServer
{
    class World
    {
        private readonly List<GameObject> gameObjects = new List<GameObject>();
        private List<GameObject> testObjects = new List<GameObject>();

        public const int DefaultObjectsCount = 500;
        private Vector3 worldMin = new Vector3(-50f, 0f, -50f);
        private Vector3 worldMax = new Vector3(50f, 0f, 50f);

        private ByteBuffer serializeBuffer;
        private ByteBuffer serializeAllBuffer;

        private const int MaxItersWhileCreateWorld = 100;
        private const float TestObjectMoveSpeed = 10f;

        private int testObjectsCount;
        private bool testPosition;
        private bool testColor;
        private bool testData;

        private const float TestModeObjectSpeed = 5;

        public World()
        {
            GenerateObjects(DefaultObjectsCount);
        }

        private Guid GetNewId()
        {
            return Guid.NewGuid();
        }

        public void GenerateObjects(int count)
        {
            var newObjects = new List<GameObject>();
            var rnd = new Random();
            int generatedCount = 0;
            for (int i = 0; i < count; ++i)
            {
                bool success = false;
                int iter = 0;
                while (!success && iter < MaxItersWhileCreateWorld)
                {
                    ++iter;
                    var pos = new Vector3(
                        (float)rnd.NextDouble() * (worldMax.X - worldMin.X) + worldMin.X,
                        worldMin.Y,
                        (float)rnd.NextDouble() * (worldMax.Z - worldMin.Z) + worldMin.Z
                        );

                    if (newObjects.FirstOrDefault(p => (p.Position - pos).SqrMagnitude() < 1f) == null)
                    {
                        var go = new GameObject(GetNewId(), pos, (byte)rnd.Next(3));
                        newObjects.Add(go);
                        ++generatedCount;
                        success = true;
                    }
                }
                if (iter >= MaxItersWhileCreateWorld)
                {
                    Console.WriteLine("Too many iterations while creating object #" + i);
                    break;
                }
            }
            lock (gameObjects)
            {                
                gameObjects.AddRange(newObjects);
            }
            Console.WriteLine($"Generated {generatedCount} objects");
        }

        public void ColorAll(byte color)
        {
            lock (gameObjects)
            {
                for (int i = 0; i < gameObjects.Count; ++i)
                {
                    gameObjects[i].Color = color;
                }
            }
        }

        public ByteBuffer GetSerializeData()
        {
            return serializeBuffer;
        }

        public ByteBuffer GetSerializeAllData()
        {
            if (serializeAllBuffer == null)
            {
                serializeAllBuffer = new ByteBuffer();
                foreach (var go in gameObjects)
                {
                    serializeAllBuffer.WriteBytes(go.GetSerializeAllBytes());
                }
            }
            return serializeAllBuffer;
        }

        public void Update()
        {
            GameObject[] objects = null;
            lock (gameObjects)
            {
                objects = gameObjects.Where(p => !p.IsSynchronized).ToArray();
            }

            serializeBuffer = new ByteBuffer();
            serializeAllBuffer = null;
            foreach (var go in objects)
            {
                if (go.IsDeleted)
                {
                    serializeBuffer.WritePackType(NetPacket.PackType.Delete);
                    serializeBuffer.WriteGuid(go.Id);
                    gameObjects.Remove(go);
                }
                else
                {
                    serializeBuffer.WriteBytes(go.GetSerializeBytes());
                }
            }
        }

        public void InitTestMode(int objectsCount, bool updatePos, bool updateColor, bool updateData)
        {
            testObjectsCount = objectsCount;
            testPosition = updatePos;
            testColor = updateColor;
            testData = updateData;

            testObjects.Clear();
            int range = Math.Min(gameObjects.Count, testObjectsCount);
            if (range < 1) return;

            lock (gameObjects)
            {
                testObjects.AddRange(gameObjects.GetRange(0, range - 1).ToArray());
            }
        }

        public void TestModeUpdate()
        {
            var rnd = new Random();
            foreach (var go in testObjects)
            {
                if (!go.IsDeleted)
                {
                    if (testColor)
                    {
                        go.Color = (byte)rnd.Next(3);
                    }
                    if (testData)
                    {
                        go.GenerateRandomData(GameObject.MaxDataSize);
                    }
                    if (testPosition)
                    {
                        var dir = new Vector3((float)rnd.NextDouble() * 2f - 1f, 0f, (float)rnd.NextDouble() * 2f - 1f);
                        dir.Normalize();
                        var pos = go.Position + dir * TestObjectMoveSpeed * (GameServer.MainThreadDelay / 1000f);
                        if (pos.X < worldMin.X) pos.X = worldMin.X;
                        if (pos.Y < worldMin.Y) pos.Y = worldMin.Y;
                        if (pos.Z < worldMin.Z) pos.Z = worldMin.Z;
                        if (pos.X > worldMax.X) pos.X = worldMax.X;
                        if (pos.Y > worldMax.Y) pos.Y = worldMax.Y;
                        if (pos.Z > worldMax.Z) pos.Z = worldMax.Z;
                        go.Position = pos;
                    }
                }
            }
        }

        public GameObject FindObject(Guid id)
        {
            lock (gameObjects)
            {
                return gameObjects.FirstOrDefault(p => p.Id.Equals(id));
            }
        }

        public GameObject[] GetAllObjects()
        {
            lock (gameObjects)
            {
                return gameObjects.ToArray();
            }
        }

        public void DeleteObject(Guid id)
        {
            var go = FindObject(id);
            if (go != null)
            {
                go.IsDeleted = true;
            }
        }

        public void DeleteAll()
        {
            lock (gameObjects)
            {
                for (int i = 0; i < gameObjects.Count; ++i)
                {
                    gameObjects[i].IsDeleted = true;
                }
            }
        }

        public GameObject AddObject(Vector3 pos, byte color)
        {
            var go = new GameObject(GetNewId(), pos, color);
            lock (gameObjects)
            {
                gameObjects.Add(go);
            }
            return go;
        }
    }
}
