﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    class User
    {
        public Guid Id { get; private set; }

        public string Name { get; set; }
        public int Password { get; set; }

        public bool NeedWorldUpdate { get; set; }
                
        public bool IsOnline { get; set; }

        public NetClient NetClient { get; set; }

        public User()
        {
            Id = Guid.NewGuid();
        }

    }
}
