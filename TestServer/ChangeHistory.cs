﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    struct Snapshot
    {
        public Guid AuthorId;
        public GameObject[] GameObjects;
    }

    class ChangeHistory
    {
        private List<Snapshot> snapshots = new List<Snapshot>();
        
        public void Commit(GameObject[] gameObjects, Guid authorId)
        {
            Task.Run(() =>
            {
                var snapshot = new Snapshot
                {
                    AuthorId = authorId,
                    GameObjects = new GameObject[gameObjects.Length]

                };
                for (int i = 0; i < gameObjects.Length; ++i)
                {
                    snapshot.GameObjects[i] = new GameObject(gameObjects[i]);
                }
                lock (snapshots)
                {                    
                    snapshots.Add(snapshot);
                }
            });
        }
    }
}
