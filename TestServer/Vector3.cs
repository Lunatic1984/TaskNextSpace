﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    struct Vector3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        
        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;            
        }

        public float SqrMagnitude()
        {
            return X * X + Y * Y + Z * Z;
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt(SqrMagnitude());
        }

        public void Normalize()
        {
            float len = Magnitude();
            X /= len;
            Y /= len;
            Z /= len;
        }

        public static Vector3 operator +(Vector3 A, Vector3 B)
        {
            return new Vector3(A.X + B.X, A.Y + B.Y, A.Z + B.Z);
        }

        public static Vector3 operator -(Vector3 A, Vector3 B)
        {
            return new Vector3(A.X - B.X, A.Y - B.Y, A.Z - B.Z);
        }

        public static Vector3 operator *(Vector3 A, float F)
        {
            return new Vector3(A.X * F, A.Y * F, A.Z *F);
        }

        public static Vector3 operator /(Vector3 A, float F)
        {
            return new Vector3(A.X / F, A.Y / F, A.Z / F);
        }
    }
}
