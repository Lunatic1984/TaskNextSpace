﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NetworkUtils;

namespace TestServer
{
    class Server
    {
        protected Network network;
        private int listenPort;

        private Timer tickTimer;

        private const int ServerTickDelay = 30;

        protected virtual bool Start(int port)
        {
            listenPort = port;
            network = new Network(listenPort);
            if (!network.Start())
            {
                network = null;
                return false;
            }

            tickTimer = new Timer(ServerTick, null, ServerTickDelay, ServerTickDelay);
            return true;
        }

        protected virtual void Stop()
        {
            if (tickTimer != null)
            {
                tickTimer.Dispose();
                tickTimer = null;
            }
            if (network != null)
            {
                network.Stop();
                network = null;
            }
        }

        private void ServerTick(object state)
        {
            NetClient[] disconnected = network.RemoveDisconnected();
            NetClient[] clients = network.GetNetClients();

            if (disconnected.Length > 0)
            {
                Console.WriteLine($"Disconnected {disconnected.Length} clients ({clients.Length})");
            }

            for (int i = 0; i < clients.Length; ++i)
            {
                var client = clients[i];
                var buf = client.Receive();
                if (buf != null)
                {
                    NetPacket.Handle(buf, client);
                    if (buf.Length > 0)
                    {
                        client.ReturnToReceiveBuffer(buf);
                    }
                }
            }
        }
    }
}
